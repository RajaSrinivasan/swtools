with Ada.Text_Io; Use Ada.Text_Io;
package body ch is
    procedure MacroNames( hfilename : string ;
                          section : string ) is
        hfile : File_Type ;
        cfile : File_Type ;
        hline : String(1..256) ;
        hlinelen : natural ;
        procedure FindBegin is
           marker : constant String := "//begin: ";
        begin
            while Not End_Of_File(hfile)
            loop
               Get_Line(hfile,hline,hlinelen) ;
               if hlinelen > marker'length + 1 and then
                  hline(1..marker'length) = marker 
               then
                  return ;
               end if ;
            end loop ;
        end FindBegin ;
        function End_Marker_Found return boolean is
           marker : constant String := "//end" ;
        begin
           if hlinelen >= marker'Length and then
              hline(1..marker'Length) = marker
           then
              return true ;
           end if ;
           return false ;
        end End_Marker_Found ;
    begin
        Open(hfile,In_File,hfilename);
        Create(cfile,Out_File,hfilename & "_" & section & ".c");

        FindBegin ;

        while Not End_Of_File(hfile)
        loop
            Get_Line(hfile,hline,hlinelen);
            if End_Marker_Found
            then
               exit ;
            end if ;
        end loop ;

        Close(hfile);
        Close(cfile);
    end MacroNames ;
end ch ;
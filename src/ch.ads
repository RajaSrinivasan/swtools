package ch is
    -- Scan file "hfilename" for sections marked by the begin/end markers
    -- to generate a name lookup
    -- //begin <name>
    -- //end 
    procedure MacroNames( hfilename : string ;
                          section : string ) ;
end ch ;
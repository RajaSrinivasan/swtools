with Ada.Command_Line; use Ada.Command_Line ;
with ch ;
procedure Swtools is
   hfilename : String := Argument(1);
   sectionname : String := Argument(2);
begin
   ch.MacroNames (hfilename,sectionname);
end Swtools;
